extends Control

@export_file("*.tscn") var file_path

func _on_play_button_pressed() -> void:
	get_tree().change_scene_to_file(file_path)


func _on_exit_button_pressed() -> void:
	get_tree().quit()
